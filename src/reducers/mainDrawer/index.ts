import { combineReducers } from "redux";
import enabled from "./enabled";
import opened from "./opened";

export default combineReducers({
    enabled, opened
});
