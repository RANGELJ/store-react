import { ActionType } from 'typesafe-actions';
import { TOOGLE_MAIN_DRAWER } from '../constants';
import { ToogleMainDrawerAction } from './actions';

export default (state = false, action: ActionType<typeof ToogleMainDrawerAction>) => {
    switch(action.type) {
        case TOOGLE_MAIN_DRAWER:
            return !state;
        default:
            return state;
    }
}
