import { ActionType } from 'typesafe-actions';
import { DISABLE_DRAWER, ENABLE_DRAWER } from '../constants';
import * as drawer from "./actions";

export default (state = true, action: ActionType<typeof drawer>) => {
    switch (action.type) {
        case ENABLE_DRAWER:
            return true;
        case DISABLE_DRAWER:
            return false;
        default:
            return state;
    }
};
