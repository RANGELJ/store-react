import { action } from 'typesafe-actions';
import { DISABLE_DRAWER, ENABLE_DRAWER, TOOGLE_MAIN_DRAWER } from '../constants';

export const EnableDrawerAction = () => action(ENABLE_DRAWER);
export const DisableDrawerAction = () => action(DISABLE_DRAWER);
export const ToogleMainDrawerAction = () => action(TOOGLE_MAIN_DRAWER);
