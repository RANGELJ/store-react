
export const ENABLE_DRAWER = "@1";

export const DISABLE_DRAWER = "@2";

export const SET_MAIN_TOOLBAR_TITLE = "@3";

export const TOOGLE_MAIN_DRAWER = "@4";
