import { ActionType } from 'typesafe-actions';
import { SET_MAIN_TOOLBAR_TITLE } from '../constants';
import { SetMainToolbarTitleAction } from './actions';

export default (state = "Money", action: ActionType<typeof SetMainToolbarTitleAction>) => {
    switch(action.type) {
        case SET_MAIN_TOOLBAR_TITLE:
            return action.payload;
        default:
            return state;
    }
};
