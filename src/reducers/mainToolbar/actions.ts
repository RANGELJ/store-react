import { action } from 'typesafe-actions';
import { SET_MAIN_TOOLBAR_TITLE } from '../constants';

export const SetMainToolbarTitleAction = (title: string) => action(SET_MAIN_TOOLBAR_TITLE, title);
