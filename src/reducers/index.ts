import { combineReducers } from "redux";
import { StateType } from 'typesafe-actions';
import mainDrawerReducer from "./mainDrawer";
import mainToolbarReducer from "./mainToolbar";

export const rootReducer = combineReducers({
    mainDrawer: mainDrawerReducer,
    mainToolbar: mainToolbarReducer,
});

export type RootState = StateType<typeof rootReducer>;
