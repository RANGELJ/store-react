import * as firebase from "firebase/app";
import "firebase/firestore";
import * as React from "react";

firebase.initializeApp({
    apiKey: "AIzaSyCudqh6i-OyT2fcUge96Wd-YjnKBEzk99M",
    authDomain: "store-rangel.firebaseapp.com",
    databaseURL: "https://store-rangel.firebaseio.com",
    messagingSenderId: "158604959125",
    projectId: "store-rangel",
    storageBucket: "store-rangel.appspot.com",
});

const firestore = firebase.firestore();

firestore.settings({
    timestampsInSnapshots: true,
});

export interface IWithFirestore {
    firestore: firebase.firestore.Firestore
}

export const withFirestore = () => {
    
    return <T,>(Element: React.ComponentType<T & IWithFirestore>) => {

        return (props: T) => {
            const newProps = {
                ...props,
                firestore
            };
            return <Element { ...newProps }/>
        };
    };
}
