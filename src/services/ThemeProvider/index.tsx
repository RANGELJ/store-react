import { createMuiTheme } from '@material-ui/core';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import * as React from 'react';

let theme = createMuiTheme({
    palette: {
        primary: {
            dark: '#006db3',
            light: '#63ccff',
            main: '#2E4172',
        },
    },
    shape: {
        borderRadius: 8,
    },
    typography: {
        h5: {
            fontSize: 26,
            fontWeight: 500,
            letterSpacing: 0.5,
        },
        useNextVariants: true,
    },
});

theme = {
    ...theme,
    mixins: {
        ...theme.mixins,
        toolbar: {
            minHeight: 48,
        },
    },
    overrides: {
        MuiAvatar: {
            root: {
                height: 32,
                width: 32,
            },
        },
        MuiButton: {
            contained: {
                '&:active': {
                    boxShadow: 'none',
                },
                boxShadow: 'none',
            },
            label: {
                textTransform: 'initial',
            },
        },
        MuiDivider: {
            root: {
                backgroundColor: '#404854',
            },
        },
        MuiDrawer: {
            paper: {
                backgroundColor: '#18202c',
            },
        },
        MuiIconButton: {
            root: {
                padding: theme.spacing.unit,
            },
        },
        MuiListItemIcon: {
            root: {
                '& svg': {
                    fontSize: 20,
                },
                color: 'inherit',
                marginRight: 0,
            },
        },
        MuiListItemText: {
            primary: {
                fontWeight: theme.typography.fontWeightMedium,
            },
        },
        MuiTab: {
            labelContainer: {
                padding: 0,
                [theme.breakpoints.up('md')]: {
                    padding: 0,
                },
            },
            root: {
                margin: '0 16px',
                minWidth: 0,
                textTransform: 'initial',
                [theme.breakpoints.up('md')]: {
                    minWidth: 0,
                },
            },
        },
        MuiTabs: {
            indicator: {
                backgroundColor: theme.palette.common.white,
                borderTopLeftRadius: 3,
                borderTopRightRadius: 3,
                height: 3,
            },
            root: {
                marginLeft: theme.spacing.unit,
            },
        },
        MuiTooltip: {
            tooltip: {
                borderRadius: 4,
            },
        },
    },
    props: {
        MuiTab: {
            disableRipple: true,
        },
    },
};

export default class ThemeProvider extends React.Component {
    public render() {
        return <MuiThemeProvider theme={theme}>
            {this.props.children}
        </MuiThemeProvider>
    }
}
