import * as React from 'react';
import MainScene from './scenes/MainScene';

export default class App extends React.Component {
    public render() {
        return (
            <div className="App">
                <MainScene/>
            </div>
        );
    }
}
