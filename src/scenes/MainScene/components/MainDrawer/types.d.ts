
export interface IMainDrawerProps {
    classes: {
        mainDrawer: string,
    }
}

export interface IMainDrawerStateProps {
    mainDrawerOpened: boolean
}
