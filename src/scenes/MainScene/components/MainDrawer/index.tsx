import { createStyles, Hidden, Theme, withStyles } from '@material-ui/core';
import * as React from "react";
import { connect } from "react-redux";
import { RootState } from 'src/reducers';
import RNavigator from './components/RNavigator';
import { IMainDrawerProps, IMainDrawerStateProps } from './types';

const MainDrawerFactory = (width: number) => {

    const styleClasses = (theme: Theme) => createStyles({
        mainDrawer: {
            [theme.breakpoints.up('sm')]: {
                flexShrink: 0,
                width,
            },
        },
    });

    const Builder = (props: IMainDrawerProps & IMainDrawerStateProps) => (
        <nav className={props.classes.mainDrawer}>
            <Hidden smUp={true} implementation="css">
                <RNavigator
                    PaperProps={{ style: { width } }}
                    variant="temporary"
                    open={props.mainDrawerOpened}/>
            </Hidden>
            <Hidden xsDown={true} implementation="css">
                <RNavigator
                    PaperProps={{ style: { width } }}/>
            </Hidden>
        </nav>
    );

    return withStyles(styleClasses)(Builder);
}

const mapStateToProps = (state: RootState) => ({
    mainDrawerOpened: state.mainDrawer.opened,
});

export default connect(mapStateToProps)(MainDrawerFactory(260));
