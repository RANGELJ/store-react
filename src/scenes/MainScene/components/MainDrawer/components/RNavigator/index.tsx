import {
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Theme,
    withStyles
} from '@material-ui/core';
import HomeIcon from '@material-ui/icons/Home';
import PeopleIcon from '@material-ui/icons/People';
import classNames from "classnames";
import * as React from 'react';
import CategoryItem, { ICategoryItem } from './components/CategoryItem';

const styles = (theme: Theme) => ({
    categoryHeader: {
        paddingBottom: 16,
        paddingTop: 16,
    },
    categoryHeaderPrimary: {
        color: theme.palette.common.white,
    },
    divider: {
        marginTop: theme.spacing.unit * 2,
    },
    firebase: {
        color: theme.palette.common.white,
        fontFamily: theme.typography.fontFamily,
        fontSize: 24,
    },
    item: {
        color: 'rgba(255, 255, 255, 0.7)',
        paddingBottom: 4,
        paddingTop: 4,
    },
    itemActionable: {
        '&:hover': {
            backgroundColor: 'rgba(255, 255, 255, 0.08)',
        },
    },
    itemActiveItem: {
        color: '#4fc3f7',
    },
    itemCategory: {
        backgroundColor: '#232f3e',
        boxShadow: '0 -1px 0 #404854 inset',
        paddingBottom: 16,
        paddingTop: 16,
    },
    itemPrimary: {
        '&$textDense': {
            fontSize: theme.typography.fontSize,
        },
        color: 'inherit',
        fontSize: theme.typography.fontSize,
    },
    textDense: {},
});

const categories: ICategoryItem[] = [
    {
        children: [
            { name: 'Productos', icon: <PeopleIcon />, active: true },
            { name: 'Database', icon: <PeopleIcon /> },
            { name: 'Storage', icon: <PeopleIcon /> },
            { name: 'Hosting', icon: <PeopleIcon /> },
            { name: 'Functions', icon: <PeopleIcon /> },
            { name: 'ML Kit', icon: <PeopleIcon /> },
        ],
        id: 'Almacen',
    },
    {
        children: [
            { name: 'Authentication', icon: <PeopleIcon />, active: true },
            { name: 'Database', icon: <PeopleIcon /> },
        ],
        id: 'Quality',
    },
];

const RNavigator = ({ classes, ...other }: any) => {
    return (<Drawer variant="permanent" {...other}>
        <List disablePadding={true}>
            <ListItem className={classNames(classes.firebase, classes.item, classes.itemCategory)}>Paperbase</ListItem>
            <ListItem className={classNames(classes.item, classes.itemCategory)}>
                <ListItemIcon>
                    <HomeIcon/>
                </ListItemIcon>
                <ListItemText classes={{primary: classes.itemPrimary}}>
                    Project Overview
                </ListItemText>
            </ListItem>
            {categories.map(category => (
                <CategoryItem key={category.id}
                    item={category}
                    classes={{
                        categoryHeader: classes.categoryHeader,
                        categoryHeaderPrimary: classes.categoryHeaderPrimary,
                        divider: classes.divider,
                        item: classes.item,
                        itemActionable: classes.itemActionable,
                        itemActiveItem: classes.itemActiveItem,
                        itemPrimary: classes.itemPrimary,
                        textDense: classes.textDense,
                    }}/>
            ))}
        </List>
    </Drawer>);
};

export default withStyles(styles)(RNavigator);
