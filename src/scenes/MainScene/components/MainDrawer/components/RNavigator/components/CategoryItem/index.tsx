import { Divider, List, ListItem, ListItemText } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import * as React from 'react';
import CategoryChild, { ICategoryChild } from '../CategoryChild';


export interface ICategoryItem {
    id: string,
    children: ICategoryChild[],
};

interface ICategoryItemProps {
    item: ICategoryItem,
    classes: {
        categoryHeader: string,
        categoryHeaderPrimary: string,
        divider: string,
        item: string,
        itemActionable: string,
        itemActiveItem: string,
        itemPrimary: string,
        textDense: string,
    },
};

interface ICategoryItemState {
    childrenOpened: boolean,
};

class CategoryItem extends React.Component<ICategoryItemProps, ICategoryItemState> {

    public state = {
        childrenOpened: false,
    }

    constructor(props: ICategoryItemProps) {
        super(props);
        this.toogleChildrenOpened = this.toogleChildrenOpened.bind(this);
    }
    
    public render() {

        const { item, classes } = this.props;
        const { childrenOpened } = this.state;

        return (<React.Fragment>
            <ListItem onClick={this.toogleChildrenOpened}
                className={classes.categoryHeader} button={true}>
                <ListItemText classes={{
                    primary: classes.categoryHeaderPrimary,
                }}>{item.id}</ListItemText>
            </ListItem>
            <Collapse in={childrenOpened} timeout="auto" unmountOnExit={true}>
                <List disablePadding={true}>
                    {item.children.map((child, index) => (
                        <CategoryChild
                            key={index}
                            classes={{
                                item: classes.item,
                                itemActionable: classes.itemActionable,
                                itemActiveItem: classes.itemActiveItem,
                                itemPrimary: classes.itemPrimary,
                                textDense: classes.textDense,
                            }}
                            child={child}/>
                    ))}
                </List>
            </Collapse>
            <Divider className={classes.divider}/>
        </React.Fragment>);
    }

    private toogleChildrenOpened() {
        this.setState((prevState) => ({childrenOpened: !prevState.childrenOpened}));
    }

};

export default CategoryItem;
