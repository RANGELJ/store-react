import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import classNames from "classnames";
import * as React from 'react';

export interface ICategoryChild {
    name: string,
    icon: JSX.Element,
    active?: boolean,
};

interface ICategoryChildProps {
    child: ICategoryChild,
    classes: {
        item: string,
        itemActionable: string,
        itemActiveItem: string,
        itemPrimary: string,
        textDense: string,
    },
};

const CategoryChild = ({
    child,
    classes,
}: ICategoryChildProps) => (
    <ListItem
        button={true}
        dense={true}
        className={classNames(
            classes.item,
            classes.itemActionable,
            child.active && classes.itemActiveItem,
        )}
    >
        <ListItemIcon>{child.icon}</ListItemIcon>
        <ListItemText classes={{
            primary: classes.itemPrimary,
            textDense: classes.textDense,
        }}>{child.name}</ListItemText>
    </ListItem>
);

export default CategoryChild;
