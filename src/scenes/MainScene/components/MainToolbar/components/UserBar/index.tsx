import { AppBar, Avatar, createStyles, Grid, Hidden, IconButton, Theme, Toolbar, Tooltip, Typography, withStyles } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from "@material-ui/icons/Notifications";
import * as React from "react";
import { IUserBarProps } from './types';

const styles = (theme: Theme) => createStyles({
    avatarButton: {
        padding: 4,
    },
    helpLink: {
        '&:hover': {
            color: theme.palette.common.white,
        },
        color: 'rgba(255, 255, 255, 0.7)',
        textDecoration: 'none',
    },
    menuButton: {
        marginLeft: -theme.spacing.unit,
    },
});

const UserBar = (props: IUserBarProps) => (
    <AppBar color="primary" position="sticky" elevation={0}>
        <Toolbar>
            <Grid container={true} spacing={8} alignItems="center">
                {props.drawerEnabled && <Hidden smUp={true}>
                    <Grid item={true}>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={ props.onToogleDrawer }
                            className={ props.classes.menuButton }>
                            <MenuIcon/>
                        </IconButton>
                    </Grid>
                </Hidden>}
                <Grid item={true} xs={true}/>
                <Grid item={true}>
                    <Typography className={ props.classes.helpLink } component="a">
                        Help
                    </Typography>
                </Grid>
                <Grid item={true}>
                    <Tooltip title="Alerts * No alerts">
                        <IconButton color="inherit">
                            <NotificationsIcon/>
                        </IconButton>
                    </Tooltip>
                </Grid>
                <Grid item={true}>
                    <IconButton color="inherit" className={ props.classes.avatarButton }>
                        <Avatar src="/static/images/avatar/1.jpg"/>
                    </IconButton>
                </Grid>
            </Grid>
        </Toolbar>
    </AppBar>
);

export default withStyles(styles)(UserBar);
