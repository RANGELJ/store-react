
export interface IUserBarProps {
    drawerEnabled: boolean,
    onToogleDrawer: () => void,
    classes: {
        menuButton: string,
        helpLink: string,
        avatarButton: string,
    }
}
