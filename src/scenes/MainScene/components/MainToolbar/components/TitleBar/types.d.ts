
export interface ITitleBarProps {
    title: string,
    classes: {
        main: string,
        button: string,
    }
}
