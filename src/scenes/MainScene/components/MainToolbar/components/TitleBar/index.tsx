import { AppBar, Button, createStyles, Grid, IconButton, Toolbar, Tooltip, Typography, withStyles } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import * as React from "react";
import { ITitleBarProps } from './types';

const styles = () => createStyles({
    button: {
        borderColor: 'rgba(255, 255, 255, 0.7)',
    },
    main: {
        zIndex: 0,
    },
});

const TitleBar = (props: ITitleBarProps) => {
    return (
        <AppBar
            component="div"
            className={props.classes.main}
            color="primary"
            position="static"
            elevation={0}>
            <Toolbar>
                <Grid container={true} alignItems="center" spacing={8}>
                    <Grid item={true} xs={true}>
                        <Typography color="inherit" variant="h5">
                            { props.title }
                        </Typography>
                    </Grid>
                    <Grid item={true}>
                        <Button
                            className={props.classes.button}
                            variant="outlined"
                            color="inherit"
                            size="small">
                            Web setup
                        </Button>
                    </Grid>
                    <Grid item={true}>
                        <Tooltip title="Help">
                            <IconButton color="inherit">
                                <HelpIcon />
                            </IconButton>
                        </Tooltip>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
};

export default withStyles(styles)(TitleBar);
