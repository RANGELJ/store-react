import { AppBar, createStyles, Tab, Tabs, withStyles } from '@material-ui/core';
import * as React from "react";
import { ITapsBarProps } from './types';

const styles = () => createStyles({
    main: {
        zIndex: 0,
    },
});

const TapsBar = (props: ITapsBarProps) => (
    <AppBar
        component="div"
        className={props.classes.main}
        color="primary"
        position="static"
        elevation={0}
    >
        <Tabs value={0} textColor="inherit">
            <Tab textColor="inherit" label="Users" />
            <Tab textColor="inherit" label="Sign-in method" />
            <Tab textColor="inherit" label="Templates" />
            <Tab textColor="inherit" label="Usage" />
        </Tabs>
    </AppBar>
);

export default withStyles(styles)(TapsBar);
