import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from "redux";
import { RootState } from 'src/reducers';
import { ToogleMainDrawerAction } from 'src/reducers/mainDrawer/actions';
import TapsBar from './components/TapsBar';
import TitleBar from './components/TitleBar';
import UserBar from './components/UserBar';

export interface IStateProps {
    dispatch: Dispatch<any>
    drawerEnabled: boolean,
    title: string,
}

const MainToolbar = (props: IStateProps) => {

    const handleToogleMainDrawer = () => {
        props.dispatch(ToogleMainDrawerAction());
    };

    return (<React.Fragment>
        <UserBar
            onToogleDrawer={ handleToogleMainDrawer }
            drawerEnabled={ props.drawerEnabled }/>
        <TitleBar title={ props.title }/>
        <TapsBar/>
    </React.Fragment>);
};

const mapStateToProps = (state: RootState) => {
    return {
        drawerEnabled: state.mainDrawer.enabled,
        title: state.mainToolbar.title,
    };
};

export default connect(mapStateToProps)(MainToolbar);
