import { createStyles, withStyles } from '@material-ui/core';
import { Theme } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import * as React from 'react';
import { connect } from 'react-redux';
import { HashRouter, Route } from 'react-router-dom';
import { RootState } from 'src/reducers';
import ThemeProvider from 'src/services/ThemeProvider';
import LoginScene from '../LoginScene';
import MainDrawer from './components/MainDrawer';
import MainToolbar from './components/MainToolbar';

const styles = ((theme: Theme) => createStyles({
    appContent: {
        display: 'flex',
        flex: 1,
        flexDirection: "column",
    },
    mainContent: {
        background: '#eaeff1',
        flex: 1,
        padding: '48px 36px 0',
    },
    root: {
        display: 'flex',
        minHeight: '100vh',
    },
}));

interface IStateProps {
    drawerEnabled: boolean,
}

interface IMainSceneProps {
    classes?: any
}

interface IMainSceneState {
    drawerOpen: boolean,
}

class MainScene extends React.Component<IMainSceneProps & IStateProps, IMainSceneState> {

    constructor(props: IMainSceneProps & IStateProps) {
        super(props);
        this.state = {
            drawerOpen: false,
        };
    }

    public render() {
        const { classes } = this.props;

        return (<ThemeProvider>
            <div className={classes.root}>
                <CssBaseline/>
                { this.props.drawerEnabled &&
                    <MainDrawer/> }
                <div className={classes.appContent}>
                    <MainToolbar/>
                    <main className={classes.mainContent}>
                        <HashRouter>
                            <React.Fragment>
                                <Route path="/" exact={true} component={LoginScene}/>
                            </React.Fragment>
                        </HashRouter>
                    </main>
                </div>
            </div>
        </ThemeProvider>)
    }
}

const mapStateToProps = (state: RootState, ownProps: IMainSceneProps): IStateProps => ({
    ...ownProps,
    drawerEnabled: state.mainDrawer.enabled,
});

export default connect( mapStateToProps )(withStyles(styles)(MainScene));
