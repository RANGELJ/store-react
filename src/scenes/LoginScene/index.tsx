import { Button, Grid } from '@material-ui/core';
import * as React from "react";
import BasicFrame from 'src/components/BasicFrame';

export default class LoginScene extends React.Component {
    public render() {

        const toolbarContent = <Grid item={true}>
            <Button variant="contained" color="primary">
                New
            </Button>
        </Grid>

        return (<BasicFrame toolbarContent={toolbarContent}>
            <Grid container={true}>
                hello
            </Grid>
        </BasicFrame>);
    }
}
