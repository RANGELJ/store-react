import {
    AppBar,
    createStyles,
    Paper,
    Theme,
    Toolbar,
    withStyles
} from '@material-ui/core';
import * as React from 'react';

const styles = ( (theme: Theme) => createStyles({
    addUser: {
        marginRight: theme.spacing.unit,
    },
    block: {
        display: 'block',
    },
    contentWrapper: {
        margin: '40px 16px',
    },
    paper: {
        margin: 'auto',
        maxWidth: 936,
        overflow: 'hidden',
    },
    searchBar: {
        borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    },
    searchInput: {
        fontSize: theme.typography.fontSize,
    },
}) );

interface IBasicFame {
    classes: {
        contentWrapper: string,
        paper: string,
        searchBar: string,
    },
    toolbarContent?: JSX.Element,
    children: JSX.Element[] | JSX.Element | string
}

const BasicFrame = ({ classes, children, toolbarContent }: IBasicFame) => (
    <Paper className={classes.paper}>
        <AppBar
            className={classes.searchBar}
            position="static"
            color="default"
            elevation={0}>
            <Toolbar>
                {toolbarContent}
            </Toolbar>
        </AppBar>
        <div className={classes.contentWrapper}>
            {children}
        </div>
    </Paper>
);

export default withStyles(styles)(BasicFrame);
